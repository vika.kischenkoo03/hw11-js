const iconPass = document.querySelectorAll(".icon_input");

iconPass.forEach(function (icon) {
  icon.onclick = function () {
    let target = this.getAttribute("data-target");
    let inputPass = document.querySelector(target);
    if (inputPass.getAttribute("type") === "password") {
      inputPass.setAttribute("type", "text");
      icon.classList.remove("active");
      icon.classList.replace("fa-eye", "fa-eye-slash");
    } else {
      inputPass.setAttribute("type", "password");
      icon.classList.add("active");
      icon.classList.replace("fa-eye-slash", "fa-eye");
    }
  };
});

let btn = document.querySelector(".btn");
btn.onclick = function () {
  let firstPassword = document.getElementById("first_password").value;
  let secondPassword = document.getElementById("second_password").value;

  event.preventDefault();
  if (!firstPassword || !secondPassword) {
    showErrorMessage("Введіть пароль");
  } else if (firstPassword === secondPassword) {
    alert("You are welcome");
  } else {
    showErrorMessage("Потрібно ввести однакові значення");
  }
};

let errorText = document.createElement("span");
errorText.classList.add("error_text");
function showErrorMessage(msg) {
    let btn = document.querySelector(".btn");
    errorText.textContent = msg;
    btn.before(errorText);
    setTimeout(() => errorText.remove(), 3000);
  };